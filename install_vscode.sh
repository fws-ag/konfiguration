#!/bin/bash

# Install Visual Studio Code Repository
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

# Update package cache and install Visual Studio Code
sudo apt install apt-transport-https
sudo apt update
sudo apt install code # or code-insiders

code --install-extension CoenraadS.bracket-pair-colorizer-2
code --install-extension ms-vscode.Go
code --install-extension PKief.material-icon-theme
code --install-extension Rubymaniac.vscode-direnv
code --install-extension shardulm94.trailing-spaces