#!/bin/bash

mkdir -p ~/Projekte/AG

sudo cp ./tools/gen_direnv /usr/local/bin/
sudo chmod u+x /usr/local/bin/gen_direnv

# Install Go, direnv
mkdir ~/go
sudo apt install -y golang-go
sudo apt install -y direnv
