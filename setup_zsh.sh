#!/bin/bash

# Install powerline fonts
mkdir ~/.fonts
cp ./config/fonts/* ~/.fonts

fc-cache -vf ~/.fonts

# Install zsh, change default shell
sudo apt install -y zsh
sudo chsh -s /bin/zsh $USER


# Install Zplug
curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh

# Configure Powerlevel9k theme
rm ~/.zshrc
cp ./config/dot.zshrc ~/.zshrc
source ~/.zshrc

zplug install
